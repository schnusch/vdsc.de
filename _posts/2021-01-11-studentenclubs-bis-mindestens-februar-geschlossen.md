---
layout: post
title:  "Studentenclubs bis mindestens Februar geschlossen"
date:   2021-01-11 13:00
author: Burts
---

Nun wurde der Teil-Lockdown erstmal weiter verlängert bis 7. Februar. Solange bleiben die Studentenclubs auf jeden Fall noch geschlossen.
Aufgrund der Erfahrungen von 2020 und der derzeitigen der Lage in Sachsen ist jedoch davon auszugehen, dass die Clubs auch dann noch nicht schnell wieder öffnen können. Hoffen wir aber das beste! Bleibt gesund!