---
layout: post
title:  "Erstes Campus Culture Festival der TU Dresden"
date:   2022-06-17 12:00
author: Burts
---
Nach dem Ende der Dresdner Studententage geht die Kultur auf dem Campus ist die nächste Runde! Am 25. Juni 2022 lädt die TU Dresden zum ersten [Campus Culture Festival][1]. Zwischen 15 und 24 Uhr wartet an fünf Standorten auf dem TU-Campus ein abwechslungsreiches Programm. Dabei werdet ihr im Görges-Bau, am Weberplatz und zwischen Bio-Gebäude und SLUB durch die Dresdner Studentenclubs gastronomisch versorgt!

 [1]: https://tu-dresden.de/tu-dresden/universitaetskultur/campus-culture-festival-2022 
