---
layout: post
title:  "Neuauflage Studentenclubheft"
date:   2017-09-01 12:00
author: Burts
---

Endlich gibt es eine [neue Auflage des Clubhefts][1]. Pünktlich vor Beginn des Wintersemesters 2017/2018 ist das Infoheft der Dresdner Studentenclubs in der sechsten Auflage erschienen. Auf 40 Seiten stellen sich die 15 Dresdner Studentenclubs kurz vor. Außerdem gibt es Gutscheine für jeden Club. Die Clubhefte liegen pünktlich zum Semesterstart in den Clubs und [hier als PDF][1].

[1]: /archiv/clubheft-2017.pdf
