---
layout: post
title:  "25. Dresdner Studententage"
date:   2016-05-02 12:00
author: Burts
---

Vom 23. Mai bis 10. Juni sind endlich wieder [Studententage][5]. Die Dresdner Studentenclubs und das Studentenwerk laden euch ein zu drei Wochen Kulturfestival der besonderen Art. Ob Konzert, Theater, Zauberei oder Party, ob gemütlicher Clubabend oder Großevent – es ist für jeden was dabei. 

[5]: https://www.dresdner-studententage.de
