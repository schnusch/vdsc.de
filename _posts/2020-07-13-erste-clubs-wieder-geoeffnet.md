---
layout: post
title:  "Erste Clubs wieder geöffnet"
date:   2020-07-13 18:00
author: Burts
---

Mit den aktuellen Verordnungen ist nun wieder zumindest ein eingeschränkter Clubbetrieb unter besonderen Hygienebedingungen möglich. So haben derzeit der [Club Aquarium][1], der [Bärenzwinger][2], der [Club11][3], der [Club Wu5][4], der [Club CountDown][5], der [Traumtänzer][6] sowie der [Heinrich-Cotta-Club][7] in Tharandt mit veränderten Öffnungszeiten geöffnet. Bitte jeweils die Zeiten und Hinweise der einzelnen Clubs beachten. Die anderen Dresdner Studentenclubs können derzeit noch nicht öffnen.

[1]: https://club-aquarium.de/
[2]: https://www.baerenzwinger.de/
[3]: http://www.clubelf.de/
[4]: http://wu5.de/
[5]: https://countdown-dresden.de/
[6]: https://club-traumtaenzer.de/
[7]: http://heinrich-cotta-club.de/
