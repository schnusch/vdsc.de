---
layout: post
title:  "Die Dresdner Studententage – es gibt sie wieder"
date:   2021-03-01 11:45
author: Burts
---
… und damit Hoffnung für die (studentische) Kulturszene. Etwas später im Jahr als üblich, sollen die diesjährigen Dresdner Studententage, das große studentische Kulturfest, mit einem kulturellen Markttreiben am 8. Juni starten. Sie werden veranstaltet vom [Studentenwerk Dresden][1] und den [Dresdner Studentenclubs][2]. Weitere Höhepunkte sind mit dem Band- und DJ-Contest [UNI AIR][3] und dem Sommerfest an der Wundtstraße geplant, bevor das [MittelAlterFest][5] der Studentenclubs GAG 18 und Traumtänzer das Festival am 27. Juni beschließt. Dazwischen und drum herum erwarten euch viele weitere Veranstaltungen. Mit Beginn des Sommersemesters wird auf der offiziellen Website der [Dresdner Studententage][5] das (komplette) Programm veröffentlicht.

[1]: https://www.studentenwerk-dresden.de/kultur/
[2]: https://vdsc.de/clubs.html
[3]: https://www.studentenwerk-dresden.de/kultur/uni-air-bandcontest.html
[4]: https://www.mittelalterfeste.eu/
[5]: https://www.dresdner-studententage.de/
