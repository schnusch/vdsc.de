---
layout: post
title:  "ESE-Clubtour"
date:   2019-09-21 22:30
author: Burts
---

Der Sommer ist zu Ende, und es geht wieder an die Uni… Für alle Erstsemester, veranstalten die Dresdner Studentenclubs zusammen mit den Fachschaften der [HTW Dresden][1] und der [TU Dresden][2] die ESE-Clubtour als Teil der Erstsemester-Einführung zum Semesterstart. Am 30.09.2019 geht‘s für die Erstis der HTW auf eine [kleine Schnuppertour][3] und am 08.10.2019 folgt die [ESE-Clubtour 2019][4] für die TU-Erstis.

[1]: https://stura.htw-dresden.de/
[2]: https://www.stura.tu-dresden.de/fachschaften
[3]: https://www.exmatrikulationsamt.de/6082130
[4]: https://www.exmatrikulationsamt.de/6082105