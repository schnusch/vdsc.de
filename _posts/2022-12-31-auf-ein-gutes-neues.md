---
layout: post
title:  "Auf ein gutes Neues!"
date:   2022-12-31 14:00
author: Burts
---

Ein ereignisreiches Jahr 2022 liegt hinter uns. Nachdem sich die Corona-Lage in diesem Jahr deutlich entspannt hat, konnten die [Dresdner Studentenclubs][1] im Jahr 2022 nicht nur in den Clubs wieder ein vielfältiges Veranstaltungsprogramm aufstellen. Erstmalig  gab es das gemeinsame veranstaltete NaWa-Festival „Bier, Bands und Spiele“ zu den Studententagen im Mai, gefolgt von mehreren gemeinsamen Getränkeständen zum dies academicus, dem Campus-Culture-Festival im Juni und der Immatrikulationsparty im Oktober sowie einer Aktion zum Semesterstart.

Wir sind natürlich bereits in der Planung für das nächste Jahr. Als Höhepunkt der Dresdner Studententage im Mai soll es auch wieder die Nachtwanderung geben.

[1]: https://vdsc.de/clubs
