---
layout: post
title:  "Studententage 2020 abgesagt"
date:   2020-04-22 17:30
author: Burts
---

Was schon absehbar war, ist nun klar: Die [Dresdner Studententage][1] samt der [Dresdner Nachtwanderung][2], dem UniAir und dem [MittelAlterFest][3] müssen aufgrund der anhaltenden Maßnahmen zur Eindämmung der COVID-19-Pandemie endgültig für das Jahr 2020 abgesagt werden. Eine Verschiebung auf einen späteren Zeitpunkt in diesem Jahr ist leider nicht möglich … Aber natürlich soll es im Mai 2021 wieder Studententage geben!

[1]: https://www.dresdner-studententage.de/
[2]: https://www.dresdner-nachtwanderung.de/
[3]: https://www.mittelalterfeste.eu/