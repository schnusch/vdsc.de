---
layout: post
title:  "Erstsemester-Clubtouren zum Semesterstart"
date:   2022-09-29 12:00
author: Burts
---

Noch von dem Vorlesungsbeginn zum Wintersemester, lassen wir die neuen Studis Clubluft schnuppern. Am 29. September führt der [StuRa][1] der HTW Dresden seine Erstis auf eine kleine ESE-Clubtour durch die Studentenclubs im Dresdner Süden, und am 4. Oktober laden dann die [Fachschaftsräte der TU Dresden][2] ihre Erstis zur ESE-Clubtour.

[1]: https://www.stura.htw-dresden.de/
[2]: https://www.stura.tu-dresden.de/fachschaften
