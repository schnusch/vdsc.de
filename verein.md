---
layout: page
title: Vereinigung Dresdner Studentenclubs e. V.
menu: Verein
order: 2
---

Die Vereinigung Dresdner Studentenclubs (VDSC) besteht seit 2007 in loser
Organisationsform als gemeinsame Interessenvertretung und Planungsrunde für
gemeinsame Aktivitäten der Dresdner Studentenclubs. 2017 wurde der Verein als
Dachverband gegründet. Die Mitglieder des VDSC sind die Vereine der Clubs.

# Gemeinnützigkeit

Der VDSC e.V. hat den gemeinnützigen Zweck, das bürgerschaftliche und soziale
Engagement der Dresdner Studentenclubs zu fördern. Näheres dazu in der
[Satzung](satzung.pdf).

Deshalb ist der VDSC e.V. berechtigt, für Spenden, die uns zur Verwendung für
diesen Zweck zugewendet werden, Zuwendungsbestätigungen nach amtlich
vorgeschriebenem Vordruck auszustellen. Dies wurde uns vom Finanzamt Dresden Süd
bescheinigt.

# Mitglieder

<ul>{% for club in site.clubs %}

  {% if club.status == "member" %}
    <li>
      <strong>{{ club.name }}</strong><br/>
      <p>{% if club.alias %}({{ club.alias}})<br/>{% endif %}{{ club.address}}<br/>
      <a href="{{ club.web }}">{{ club.web | remove:'http://' | remove:'https://' }}</a></p>
    </li>
  {% endif %}

{% endfor %}</ul>


# Assoziierte Mitglieder

<ul>{% for club in site.clubs %}

  {% if club.status == "associate" %}
    <li>
      <strong>{{ club.name }}</strong><br/>
      <p>{% if club.alias %}({{ club.alias}})<br/>{% endif %}{{ club.address}}<br/>
      <a href="{{ club.web }}">{{ club.web | remove:'http://' | remove:'https://' }}</a></p>
    </li>
  {% endif %}

{% endfor %}</ul>
