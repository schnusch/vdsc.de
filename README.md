# Webseite des VDSC

Dieses Repository ist die Datenbasis für die [Webseite des VDSC][1].

Alle Inhalte sind in Textdateien gespeichert und im [Markdown][2]-Format
geschrieben. Die Dateien erkennt man an den `.md`-Endungen.

Neue Posts für die Webseite sind ebenfalls Markdown-Dateien. Sie müssen ins
Verzeichnis `_posts` gespeichert werden und ihr Dateiname muss die Form 
`yyyy-mm-dd-ueberschrift.md` haben.

Am Anfang der Textdatei für einen Post müssen das Layout, der Titel des Posts,
das Datum und der Autor im "YAML-Frontmatter" in folgendem Format angegeben
werden (Beispiel):

    ---
    layout: post
    title:  "ESE-Clubtour"
    date:   2018-09-24 12:00
    author: Burts
    ---

Die Clubs sind analog zu den Posts als Textdatei im Verzeichnis `_clubs`und den
entsprechenden Daten im folgenden Format als YAML-Frontmatter hinterlegt:

    ---
    name: Studentenclub Wu5 e.V.
    address: August-Bebel-Straße 12, 01219 Dresden
    web: http://wu5.de
    lat: 51.0322532
    lon: 13.7516466
    status: member
    ---

Wichtig für die [Clubliste][3] ist die Statusangabe `status: member`, um als
Mitglied, bzw. `status: associate` um als assoziiertes Mitglied gelistet zu
werden.

[1]: https://vdsc.de
[2]: https://git.agdsn.de/help/user/markdown.md
[3]: https://vdsc.de/clubs/

### Lokales Bearbeiten mit Docker

    docker run \
        --rm \
        --name vdsc_jekyll \
        --env "TZ=Europe/Berlin" \
        --publish 4000:4000 \
        --volume $(pwd):/srv/jekyll \
        jekyll/jekyll:4.2.2 \
        jekyll serve
