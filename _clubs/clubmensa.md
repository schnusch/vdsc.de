---
    name: Club Mensa e.V.
    address: Reichenbachstraße 1, 01069 Dresden
    city: Dresden
    web: http://www.clubmensa.de
    logo_light: assets/logos/clubmensa/light.png
    logo_dark: assets/logos/clubmensa/dark.png
    lat: 51.0340709
    lon: 13.7339909
---
