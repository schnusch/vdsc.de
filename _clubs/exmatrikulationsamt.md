---
    alias: Studentenforum Dresden e.V.
    name: eXmatrikulationsamt.de
    address: Manitiusstr. 7, 01067 Dresden (Postadresse)
    city: online
    web: https://www.exmatrikulationsamt.de
    logo_light: assets/logos/exmatrikulationsamt/light.svg
    logo_dark: assets/logos/exmatrikulationsamt/dark.svg
---
