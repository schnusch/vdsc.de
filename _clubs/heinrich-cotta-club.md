---
    name: Heinrich-Cotta-Club e.V.
    address: Wilsdruffer Straße 20, 01737 Tharandt
    city: Tharandt
    web: http://heinrich-cotta-club.de
    lat: 50.9913537
    lon: 13.5776665
    status: member
---
