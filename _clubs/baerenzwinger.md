---
    name: Studentenclub Bärenzwinger e.V.
    address: Brühlscher Garten 1, 01067 Dresden
    city: Dresden
    web: https://www.baerenzwinger.de
    logo_light: assets/logos/baerenzwinger/light.png
    logo_dark: assets/logos/baerenzwinger/dark.png
    lat: 51.0524493
    lon: 13.7451854
    status: member
---
