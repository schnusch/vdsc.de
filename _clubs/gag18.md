---
    name: Kellerklub GAG 18 e.V.
    address: Fritz-Löffler-Straße 16, 01069 Dresden
    city: Dresden
    web: https://www.gag-18.com
    logo_light: assets/logos/gag18/light.png
    logo_dark: assets/logos/gag18/dark.png
    lat: 51.0359776
    lon: 13.7313508
    status: member
---
