---
    name: Studentenclub Borsi 34 e.V.
    address: Borsbergstraße 34, 01309 Dresden
    city: Dresden
    web: http://borsi34.de
    logo_light: assets/logos/borsi34/light.png
    logo_dark: assets/logos/borsi34/dark.png
    lat: 51.0437010
    lon: 13.7804837
    status: member
---
