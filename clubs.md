---
layout: page
title: Liste der Dresdner Studentenclubs
menu: Clubs
redirect_from:
  - /clubs/
order: 1
cities:
  - Dresden
  - Tharandt
  - Görlitz
  - Zittau
  - online
append_head: |
  <link rel="stylesheet" href="assets/leaflet/leaflet.css" />
  <script src="assets/leaflet/leaflet.js"></script>
---

<div id="map"></div>
<script>
  (() => {
    const root = document.getElementById("map");

    const map = L.map(root);
    L.tileLayer("https://tile.osmand.net/hd/{z}/{x}/{y}.png", {
      maxZoom: 19,
      attribution: "<a href=\"https://osmand.net/\">OsmAnd</a> | <a href=\"https://www.openstreetmap.org/\">OSM</a>"
    }).addTo(map);

    function mark(lat, lon, popup) {
      console.log(L.marker([lat, lon]).addTo(map).bindPopup(popup));
    }
    {%- for club in site.clubs -%}
      {%- if club.lat and club.lon -%}
        {%- capture popup -%}
          <a href="{{ club.web }}">
            {{- "" -}}
            <div class="logo-popup">
              {%- if club.logo -%}
                <img src="{{ club.logo }}" alt="{{ club.name }} Logo"/>
              {%- elsif club.logo_light and club.logo_dark -%}
                <picture>
                  <source srcset="{{ club.logo_dark }}" media="(prefers-color-scheme: dark)"/>
                  <img src="{{ club.logo_light }}" alt="{{ club.name }} Logo"/>
                </picture>
              {%- elsif club.logo_light -%}
                <img class="light" src="{{ club.logo_light }}" alt="{{ club.name }} Logo"/>
              {%- elsif club.logo_dark -%}
                <img class="dark" src="{{ club.logo_dark }}" alt="{{ club.name }} Logo"/>
              {%- endif -%}
              {{- club.name -}}
            </div>
            {{- "" -}}
          </a>
        {%- endcapture %}
    mark({{ club.lat | jsonify }}, {{ club.lon | jsonify }}, {{ popup | normalize_whitespace | jsonify }});
      {%- endif -%}
    {%- endfor %}

    root.style.display = "block";

    // focus map on Dresden's clubs
    const lats = [
    {%- for club in site.clubs -%}
      {%- if club.city == "Dresden" -%}
      {{ club.lat | jsonify }},
      {%- endif -%}
    {%- endfor -%}
    ];
    const lons = [
    {%- for club in site.clubs -%}
      {%- if club.city == "Dresden" -%}
      {{ club.lon | jsonify }},
      {%- endif -%}
    {%- endfor -%}
    ];
    map.fitBounds(
      L.latLngBounds(
        L.latLng(Math.min(...lats), Math.min(...lons)),
        L.latLng(Math.max(...lats), Math.max(...lons)),
      )
    );
  })();
</script>

{% for city in page.cities %}
# {{ city }}

<ul>{% for club in site.clubs %}

  {% if club.city == city %}
    <li>
      <div class="with-clublogo">
        <div>
          <strong>{{ club.name }}</strong><br/>
          <p>{% if club.alias %}({{ club.alias}})<br/>{% endif %}{{ club.address}}<br/>
          <a href="{{ club.web }}">{{ club.web | remove:'http://' | remove:'https://' }}</a></p>
        </div>
        {% if club.logo or club.logo_light or club.logo_dark %}
          <div class="clublogo-outer-wrapper">
            <div class="clublogo-inner-wrapper">
              <a href="{{ club.web }}">
                {% if club.logo %}
                  <img src="{{ club.logo }}" alt="{{ club.name }} Logo"/>
                {% elsif club.logo_light and club.logo_dark %}
                  <picture>
                    <source srcset="{{ club.logo_dark }}" media="(prefers-color-scheme: dark)"/>
                    <img src="{{ club.logo_light }}" alt="{{ club.name }} Logo"/>
                  </picture>
                {% elsif club.logo_light %}
                  <img class="light" src="{{ club.logo_light }}" alt="{{ club.name }} Logo"/>
                {% elsif club.logo_dark %}
                  <img class="dark" src="{{ club.logo_dark }}" alt="{{ club.name }} Logo"/>
                {% endif %}
              </a>
            </div>
          </div>
        {% endif %}
      </div>
    </li>
  {% endif %}

{% endfor %}</ul>
{% endfor %}

Informationen zu Studentenclubs in anderen Städten findest du auf [studentenclubs.net](https://studentenclubs.net/).
