---
redirect_from:
  - /eseclubtour/
  - /ese/
  - /ese.html
redirect_to: https://www.exmatrikulationsamt.de/eseclubtour
---
